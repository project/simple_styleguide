<?php

namespace Drupal\Tests\simple_styleguide\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test installation and uninstallation.
 *
 * @group simple_styleguide
 */
class InstallTest extends KernelTestBase {

  private const MODULE_NAME = 'simple_styleguide';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'simple_styleguide',
  ];

  /**
   * Test that the module can be installed and uninstalled.
   */
  public function testInstallUninstall(): void {
    // Ensure the module is installed.
    $this->assertTrue(\Drupal::moduleHandler()->moduleExists(self::MODULE_NAME), 'The simple_styleguide module is installed.');

    // Uninstall the module.
    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);
    $this->assertFalse(\Drupal::moduleHandler()->moduleExists(self::MODULE_NAME), 'The simple_styleguide module is uninstalled.');

    // Reinstall the module.
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);
    $this->assertTrue(\Drupal::moduleHandler()->moduleExists(self::MODULE_NAME), 'The simple_styleguide module is reinstalled.');
  }

}
