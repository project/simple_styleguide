<?php

namespace Drupal\Tests\simple_styleguide\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group simple_styleguide
 */
class AnonymousTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['simple_styleguide'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testMainPage() {
    $this->drupalGet(Url::fromRoute('simple_styleguide.controller'));
    $this->assertSession()->statusCodeEquals(403);
  }

}
